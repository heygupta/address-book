import React,{Fragment,useEffect} from 'react';
import {BrowserRouter as Router,Route,Switch} from 'react-router-dom';
import Navbar from './components/layout/Navbar';
import Landing from './components/layout/Landing';

import './App.css';
import {loadUser} from './actions/auth';

import {Provider} from 'react-redux';
import store from './store';
import setAuthToken from '../src/utils/setAuthToken';
import Routes from './components/routing/Routes';

if(localStorage.token){
  setAuthToken(localStorage.token);
}
const App = () => {
  useEffect(()=>{
    store.dispatch(loadUser());
},[]);
  return (
    <Provider store={store}>
    <Router>
      <Fragment>
        <Navbar></Navbar>
        <Switch>
        <Route exact path="/" component={Landing}></Route>
        <Route component={Routes}></Route>
        </Switch>
        
      </Fragment>
    </Router>
    </Provider>
  )
}

export default App
