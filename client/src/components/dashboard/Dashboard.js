import React,{useEffect,Fragment} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getCurrentProfile} from '../../actions/profile';
import Spinner from '../layout/Spinner';
import DashboardAction from './DashboardAction.js';
import Contact from './Contact'
/* import Experience from './Experience';
import Education from './Education' */
const Dashboard = ({getCurrentProfile,auth:{user},profile:{profile,loading}}) => {

    useEffect(() =>{
        getCurrentProfile();
    },[getCurrentProfile]);
    
    return loading && profile === null ? <Spinner/> : <Fragment>
        <h1 className="large text-primary">Dashboard</h1>
        <p className="lead">
        <i className="fas fa-user"></i> Welcome {user && user.name}</p>
        
        <Fragment> <DashboardAction/>{ profile && profile.data && profile.data.length > 0 && <Contact contact={profile.data}/> }

        </Fragment>
        
    </Fragment>
}

Dashboard.propTypes = {
    getCurrentProfile:PropTypes.func.isRequired,
    auth:PropTypes.object.isRequired,
    profile:PropTypes.object.isRequired,
}

const mapStateToProps = state => ({
    auth:state.auth,
    profile:state.profile,
});

export default connect(mapStateToProps,{getCurrentProfile })(Dashboard);
