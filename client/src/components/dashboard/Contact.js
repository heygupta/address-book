import React ,{Fragment} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {deleteContact} from '../../actions/profile'
import {Link} from 'react-router-dom'
const Contact = ({contact,deleteContact}) => {
    
    const contacts = contact.map(exp => (
        <tr key={exp._id}>
            <td>{exp.addressBookTitle}</td>
            <td className="hide-sm">{exp.contactPersonName}</td>
            <td className="hide-sm">{exp.contactPersonNumber}</td>
            <td>
                <button onClick = {e => deleteContact(exp._id)} className="btn btn-danger">Delete</button>
            </td>
            { <td>
                <Link className="btn btn-primaryr" to={`/editContact/${exp._id}`}>Edit</Link>
            </td> }
        </tr>
    ));
    return (
        <Fragment>
            <h2 className="my-2">Contact Details</h2>
            <table className="table">
                <thead>
                    <tr>
                        <th>Contact Type</th>
                        <th className="hide-sm">Person Name</th>
                        <th className="hide-sm">Mobile Number</th>
                    </tr>
                </thead>
                <tbody>{contacts}</tbody>
            </table>
        </Fragment>
    )
}

Contact.propTypes = {
    contact:PropTypes.array.isRequired,
    delete:PropTypes.func.isRequired,
}

export default connect(null,{deleteContact})(Contact);
