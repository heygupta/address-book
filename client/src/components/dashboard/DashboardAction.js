import React from 'react';
import {Link} from 'react-router-dom';
const DashboardAction = () => {
    return (
        <div className="dash-buttons">
        <Link to="/addContact" className="btn btn-light"
          ><i className="fas fa-user-circle text-primary"></i> Add Contact</Link>
    
      </div>
    )
}

export default DashboardAction
