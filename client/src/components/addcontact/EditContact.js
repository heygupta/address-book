import React,{useState,Fragment,useEffect} from 'react'
import {withRouter,Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {getContact,updateContact} from '../../actions/profile';
import Spinner from '../layout/Spinner';
const EditContact = ({getContact,updateContact,match,profile:{contact,loading},history}) => {

  const [formData,setFormData] = useState({
    addressBookTitle:"",
    contactPersonName:"",
    contactPersonNumber:"",
    addressline1:"",
    addressline2:"",
    addressline3:"",
    pincode:"",
    city:"",
    state:"",
    country:""
});  
const [updateFormFlag,setUpdateFormFlag] = useState(true)
const { addressBookTitle,
  contactPersonName,
  contactPersonNumber,
  addressline1,
  addressline2,
  addressline3,
  pincode,
  city,
  state,
  country} = formData; 

  useEffect(()=>{
         getContact(match.params.id);
         
    },[getContact,loading])
    

  if(typeof contact != "undefined" && updateFormFlag){
    setFormData({
      addressBookTitle : loading || !contact || !contact.addressBookTitle ? "" :  contact.addressBookTitle,
      contactPersonName: loading || !contact ||  !contact.contactPersonName ? "" :contact.contactPersonName,
      contactPersonNumber:loading || !contact || !contact.contactPersonNumber ?"" :contact.contactPersonNumber,
      addressline1: loading || !contact || !contact.addressline1 ? "": contact.addressline1,
      addressline2: loading || !contact || !contact.addressline2 ? "" : contact.addressline2,
      addressline3: loading || !contact || !contact.addressline3 ? "" : contact.addressline3,
      pincode:loading || !contact || !contact.pincode? "" : contact.pincode,
      city:loading || !contact || !contact.city? "" : contact.city,
      state :loading || !contact || !contact.state? "" : contact.state,
      country:loading || !contact || !contact.country ? "" : contact.country

    });
    setUpdateFormFlag(false)
  }

    const onChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name] : e.target.value
        })
    }

    const onSubmit = e =>{
        e.preventDefault();
        updateContact(match.params.id,formData,history);

    }
    return (
        loading || typeof contact == "undefined" ? <Spinner/>:(
        
        <Fragment>
             <h1 className="large text-primary">
       Update Contact
      </h1>
      <p className="lead">
        <i className="fas fa-user"></i> Fill the form 
      </p>
      <small>* = required field</small>
      <form className="form" onSubmit={e=>onSubmit(e)}> 
      
        <div className="form-group">
          <input required type="text" placeholder="Address Book Title (Home,Work, etc.)" name="addressBookTitle" value={addressBookTitle} onChange = {e => onChange(e)} />
        </div>
        <div className="form-group">
          <input required type="text" placeholder="Contact Person Name" name="contactPersonName" value={contactPersonName} onChange = {e => onChange(e)} />
        </div>
        <div className="form-group">
          <input required  type="text" pattern="^[0-9]*$" maxLength={10} placeholder="Contact Person Number" name="contactPersonNumber" value={contactPersonNumber} onChange = {e => onChange(e)} />
        </div>
        <div className="form-group">
          <input required type="text" placeholder="Address Line 1" name="addressline1"  value={addressline1 } onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input  type="text" placeholder="Address Line 2" name="addressline2"  value={addressline2  } onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input  type="text" placeholder="Address Line 3" name="addressline3"  value={addressline3 } onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input  required type="text" pattern="^[0-9]*$" maxLength={6} placeholder="Pincode" name="pincode"  value={pincode } onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input required type="text" placeholder="City" name="city"  value={city } onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input required type="text" placeholder="State" name="state"  value={state} onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input required type="text" placeholder="Country" name="country"  value={country } onChange = {e => onChange(e)}/>
        </div>
       
       

        
        <input value="Update" type="submit" className="btn btn-primary my-1" />
        <Link className="btn btn-light my-1" to="/dashboard">Go Back</Link>
      </form>
        </Fragment>)
    )
};

EditContact.propTypes = {
    getContact:PropTypes.func.isRequired,
    updateContact:PropTypes.func.isRequired,
    profile:PropTypes.object.isRequired,
    
};
const mapStateToProps = state => ({
    auth:state.auth,
    profile:state.profile,

});


export default connect(mapStateToProps,{getContact,updateContact})(withRouter(EditContact));
