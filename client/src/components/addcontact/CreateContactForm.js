import React,{useState,Fragment} from 'react'
import {withRouter,Link} from 'react-router-dom';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';
import {addContact} from '../../actions/profile';
const CreateProfile = ({contactId,addContact,history}) => {
    const [formData,setFormData] = useState({
        addressBookTitle:"",
        contactPersonName:"",
        contactPersonNumber:"",
        addressline1:"",
        addressline2:"",
        addressline3:"",
        pincode:"",
        city:"",
        state:"",
        country:""

    });


    const { addressBookTitle,
    contactPersonName,
    contactPersonNumber,
    addressline1,
    addressline2,
    addressline3,
    pincode,
    city,
    state,
    country} = formData; 

    const onChange = (e) => {
        setFormData({
            ...formData,
            [e.target.name] : e.target.value
        })
    }
    console.log(history);
    const onSubmit = e =>{
        e.preventDefault();
        addContact(formData,history);

    }
    return (
        <Fragment>
             <h1 className="large text-primary">
       Add New Contact
      </h1>
      <p className="lead">
        <i className="fas fa-user"></i> Fill the form 
      </p>
      <small>* = required field</small>
      <form className="form" onSubmit={e=>onSubmit(e)}> 
      
        <div className="form-group">
          <input required type="text" placeholder="Address Book Title (Home,Work, etc.)" name="addressBookTitle" value={addressBookTitle} onChange = {e => onChange(e)} />
        </div>
        <div className="form-group">
          <input required type="text" placeholder="Contact Person Name" name="contactPersonName" value={contactPersonName} onChange = {e => onChange(e)} />
        </div>
        <div className="form-group">
          <input required  type="text" pattern="^[0-9]*$" maxLength={10}  placeholder="Contact Person Number" name="contactPersonNumber" value={contactPersonNumber} onChange = {e => onChange(e)} />
        </div>
        <div className="form-group">
          <input required type="text" placeholder="Address Line 1" name="addressline1"  value={addressline1} onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input  type="text" placeholder="Address Line 2" name="addressline2"  value={addressline2} onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input  type="text" placeholder="Address Line 3" name="addressline3"  value={addressline3} onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input required type="text" pattern="^[0-9]*$" placeholder="Pincode" name="pincode" maxLength={6} value={pincode} onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input required type="text" placeholder="City" name="city"  value={city} onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input required type="text" placeholder="State" name="state"  value={state} onChange = {e => onChange(e)}/>
        </div>
        <div className="form-group">
          <input required type="text" placeholder="Country" name="country"  value={country} onChange = {e => onChange(e)}/>
        </div>
       
       

        
        <input type="submit" className="btn btn-primary my-1" />
        <Link className="btn btn-light my-1" to="/dashboard">Go Back</Link>
      </form>
        </Fragment>
    )
};

CreateProfile.propTypes = {
    addContact:PropTypes.func.isRequired,
};


export default connect(null,{addContact})(withRouter(CreateProfile));
