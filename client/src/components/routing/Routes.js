import React from 'react'
import {Route,Switch} from 'react-router-dom';
 import Dashboard from '../../components/dashboard/Dashboard';
import CreateContactForm from '../../components/addcontact/CreateContactForm'
import EditContact from '../../components/addcontact/EditContact';
import NotFound from '../../components/routing/NotFound';
import PrivateRoute from '../..//components/routing/PrivateRoute';
import Register from '../../components/auth/Register';
import Login from '../../components/auth/Login';
import Alert from '../../components/layout/Alert'

const Routes = () => {
    return (
        <section className="container">
        <Alert></Alert>
          <Switch>
          <Route exact path="/login" component={Login}></Route>
          <Route exact path="/register" component={Register}></Route> 
          <PrivateRoute exact path="/dashboard" component={Dashboard}/>
          <PrivateRoute exact path="/addContact" component={CreateContactForm}></PrivateRoute>
          <PrivateRoute exact path="/editContact/:id" component={EditContact}></PrivateRoute>
          <Route component={NotFound}></Route>
          </Switch>
        </section>
    )
}

export default Routes
