import axios from 'axios';
import {setAlert} from './alert';
import { GET_PROFILE ,PROFILE_ERROR ,GET_PROFILES,UPDATE_CONTACT} from './types';


// GET current users profile

export const getCurrentProfile = () => async dispatch => {
    try {
        const res =  await axios.get('/api/profile/me');
        dispatch({
            type:GET_PROFILE,
            payload:res.data
        });
    } catch (error) {
        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        }) ;
    }
}

// Get profile by ID
export const getProfileById = userId => async dispatch => {
    try {
        const res =  await axios.get(`/api/profile/user/${userId}`);
        dispatch({
            type:GET_PROFILE,
            payload:res.data
        });
    } catch (error) {
        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        }) ;
    }
};

export const addContact = (formData,history) => async dispatch => {
    try {
        const config = {
            headers : {
                'Content-type':'application/json',
            }
        }
        const res = await axios.post('/api/profile/add',formData,config);
        dispatch({
            type:GET_PROFILES,
            payload:res.data
        });
        history.push('/dashboard');
        dispatch(setAlert("New Contact Created",'success'));

    } catch (error) {
        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        }) ;
    }
}

export const deleteContact = (id) => async dispatch => {
    try {
        await axios.delete(`/api/profile/delete/${id}`);
        dispatch(setAlert( "Contact Deleted",'danger'));
        const res =  await axios.get('/api/profile/me');
        dispatch({
            type:GET_PROFILE,
            payload:res.data
        });
    } catch (error) {
        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        }) ;
    }
}
export const getContact = (id) => async dispatch => {
    try {
        let res  = await axios.get(`/api/profile/contact/${id}`);
        dispatch({
            type:UPDATE_CONTACT,
            payload:res.data
        });
    } catch (error) {
        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        }) ;
    }
}

export const updateContact = (id,formData,history) => async dispatch => {
    try {
        const config = {
            headers : {
                'Content-type':'application/json',
            }
        }
        await axios.post(`/api/profile/update/${id}`,formData,config);
        history.push('/dashboard');
        dispatch(setAlert("Updated Success",'success'));
    } catch (error) {
        dispatch({
            type:PROFILE_ERROR,
            payload:{msg:error.response.statusText,status:error.response.status}
        }) ;
    }
}
