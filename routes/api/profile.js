const express = require('express');
const router = express.Router();
const auth = require('../../middleware/auth');
const Profile = require('../../models/Profile');
const User = require('../../models/User');
const {check ,validationResult} = require('express-validator/check');

// add contact
router.post('/add',[auth,[
    check('addressBookTitle', 'Address Book Title is required')
    .not()
    .isEmpty(),
    check('contactPersonName', 'Contact Person Name is required')
    .not()
    .isEmpty(),
    check('contactPersonNumber', 'Contact Person Number is required')
    .not()
    .isEmpty(),
    check('addressline1', 'Address Line 1 is required')
    .not()
    .isEmpty(),
    check('pincode', 'Pincode is required')
    .not()
    .isEmpty(),
    check('city', 'City Name is required')
    .not()
    .isEmpty(),
    check('state', 'state Name is required')
    .not()
    .isEmpty(),
    check('country', 'Country Name is required')
    .not()
    .isEmpty(),
    ]
        ],async(req,res) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(400).json({errors:errors.array()});
            }
    try {
        let profile = {};
        profile = {...req.body}
        profile.user = req.user;
        profile = new Profile(profile);
        await profile.save();
        res.json(profile);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server error');
    }    
});


router.post('/update/:id',[auth,[
    check('addressBookTitle', 'Address Book Title is required')
    .not()
    .isEmpty(),
    check('contactPersonName', 'Contact Person Name is required')
    .not()
    .isEmpty(),
    check('contactPersonNumber', 'Contact Person Number is required')
    .not()
    .isEmpty(),
    check('addressline1', 'Address Line 1 is required')
    .not()
    .isEmpty(),
    check('pincode', 'Pincode is required')
    .not()
    .isEmpty(),
    check('city', 'City Name is required')
    .not()
    .isEmpty(),
    check('state', 'state Name is required')
    .not()
    .isEmpty(),
    check('country', 'Country Name is required')
    .not()
    .isEmpty(),
    ]
        ],async(req,res) => {
            const errors = validationResult(req);
            if(!errors.isEmpty()){
                return res.status(400).json({errors:errors.array()});
            }
    try {
        let profile = {};
        profile = {...req.body}
      //  profile.user = req.user;
        profile = await Profile.findOneAndUpdate({_id:req.params.id},
            { $set: profile },
            {new:true}   )
        res.json(profile);



    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server error');
    }    
});

router.delete("/delete/:id",auth,async(req,res) => {
    try {
        await Profile.findOneAndDelete({_id:req.params.id});
        return res.json({msg:"Contact Delete"});
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error');
    }
});

router.get("/me",auth,async(req,res) => {
    try {
        let contact = await Profile.find({ user: req.user });
        return res.json({data:contact});
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error');
    }
})
router.get("/contact/:id",auth,async(req,res) => {
    try {
        let contact = await Profile.findById({_id:req.params.id});
        return res.json(contact);
    } catch (error) {
        console.error(error.message);
        res.status(500).send('Server Error');
    }
});



module.exports = router;