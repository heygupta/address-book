const express = require('express');
const router = express.Router();
const {check ,validationResult} = require('express-validator/check');
const bcrypt = require('bcryptjs');
const User = require('../../models/User');
const config = require('config');
const jwt = require('jsonwebtoken');
// @route POST api/users
// @access Public

router.post('/',[
    check('name','Name is required').not().isEmpty(),
    check('email','Please include a valid email').isEmail(),
    check('password','Please enter a password with 6 or mode characters').isLength({min:6})

],async (req,res) => {
    const errors = validationResult(req);
    if(!errors.isEmpty()){
        return res.status(400).json({errors:errors.array()});
    }
    const {name,email,password} = req.body;
    try {
        let user = await User.findOne({email});

        if(user){
            return res.status(400).json({errors:[{message:'User already exists'}]});
        }

        user = new User({
            name,
            email,
            password
        });

        const salt = await bcrypt.genSalt(10);
        user.password = await bcrypt.hash(password,salt);
        await user.save();
        const payload = {
            id:user.id,
        }
        jwt.sign(payload,config.get('jwtSecret'),{
            expiresIn:36000
        },(error,token)=>{
            if(error) throw error;
            res.json({token:token})
        });
        
    } catch (error) {
        console.error(error.message);
        res.status('Server error');
    }
});


module.exports = router;