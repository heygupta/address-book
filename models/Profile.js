const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const ProfileSchema = new Schema({
    user:{
        type:Schema.Types.ObjectId,
        ref:'user'
    },
    addressBookTitle:{
        type:String,
        required:true
    },
    contactPersonName:{
        type:String,
        required:true
    },
    contactPersonNumber:{
        type:String,
        required:true
    },
    addressline1:{
        type:String,
        required:true
    },
    addressline2:{
        type:String,
    },
    addressline3:{
        type:String,
    },
    pincode:{
        type:String,
        required:true
    },
    city:{
        type:String,
        required:true
    },
    state:{
        type:String,
        required:true
    },
    country:{
        type:String,
        required:true
    },
    date: {
        type: Date,
        default: Date.now
    }
})
module.exports = Profile = mongoose.model('profile',ProfileSchema);
   